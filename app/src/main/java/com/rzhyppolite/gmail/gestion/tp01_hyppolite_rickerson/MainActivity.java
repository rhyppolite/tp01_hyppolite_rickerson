package com.rzhyppolite.gmail.gestion.tp01_hyppolite_rickerson;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button buttonToast;
    private Button buttonInc;
    private TextView textViewCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
    }


    private void initViews() {
        buttonToast = findViewById(R.id.toast);
        buttonInc = findViewById(R.id.increment);
        textViewCount = findViewById(R.id.textCounter);
        final int[] counter = {0};


        buttonToast.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                //incrémenter la valeur du compteur puis mettre à jour le text
                textViewCount.setText(""+ counter[0]);
                Toast.makeText(MainActivity.this, textViewCount.getText(), Toast.LENGTH_SHORT).show();
            }
        });

        buttonInc.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                counter[0]++;
            }
        });
    }

}